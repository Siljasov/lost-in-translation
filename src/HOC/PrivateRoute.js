import { Route, Redirect } from 'react-router-dom';
import {AppRoutes} from "../const/AppRoutes";
import {LocalStorage} from "../util/localstorage";
import {LOGIN_KEY} from "../Login/Login";

export const PrivateRoute = props => {
    console.log(LocalStorage.get(LOGIN_KEY))
    if (LocalStorage.get(LOGIN_KEY) === null)
        return <Redirect to={AppRoutes.Login} exact/>

    return <Route {...props}/>
}

export default PrivateRoute;