import './Login.css';
import {LocalStorage} from "../util/localstorage";
import {useHistory} from "react-router-dom";
import {useState} from "react";
import {AppRoutes} from "../const/AppRoutes";
import Navbar from "../Navbar/Navbar";
export const LOGIN_KEY = 'u128-l-t';

function Login()
{
    const history = useHistory();
    const [error, setError] = useState();

    const onSubmitClick = () => {
        const input = document.getElementById("username");
        if (input.value === '')
            setError("Please enter a username!");
        else
        {
            LocalStorage.set(LOGIN_KEY, {username: input.value, translations: []});
            history.push(AppRoutes.Translate);
        }
    }
    return(
        <main>
            <div>
                <Navbar/>
                <input className={"inputUser"} id={"username"} placeholder={"Enter your username..."}/>
                <button className={"submit"} type={"button"} onClick={onSubmitClick}>Submit</button>
                {error && <p>{error}</p>}
            </div>
        </main>
    )
}

export default Login;