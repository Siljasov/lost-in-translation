import './navbar.css';
import {AppRoutes} from "../const/AppRoutes";
import {NavLink} from 'react-router-dom';
import {LocalStorage} from "../util/localstorage";
import {LOGIN_KEY} from "../Login/Login";

function Navbar()
{

    return(
        <main className={"main"}>
            <div>
                <header>
                    <ul className={"ulist"}>
                        <li className={"list"}>
                            <b className={"title"}>Lost in Translation</b>
                        </li>
                        <li className={"list"}>
                            <NavLink class={"profile"} to={AppRoutes.Profile}><a>{LocalStorage.get(LOGIN_KEY) ? "Signed in as: " + LocalStorage.get(LOGIN_KEY).username : "Sign In"}</a></NavLink>
                        </li>
                    </ul>
                </header>
            </div>
        </main>
    );
}

export default Navbar;