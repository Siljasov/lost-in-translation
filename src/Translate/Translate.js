import './Translate.css'
import {useState} from "react";
import {Translator} from "../util/translator";
import {LocalStorage} from "../util/localstorage";
import {LOGIN_KEY} from "../Login/Login";
import Navbar from "../Navbar/Navbar";

function Translate()
{
    const [images, setImages] = useState();
    const [error, setError] = useState();
    function translate()
    {
        const translateInput = document.getElementById("translate-input");
        if (translateInput.value.length > 40 || translateInput.value.length === 0)
        {
            setError("Please enter text between 0 and 40 characters!");
            setImages([]);
        }
        else
        {
            setError("");
            const user = LocalStorage.get(LOGIN_KEY);
            if (user.translations.length >= 10)
            {
                user.translations.shift();
                user.translations.push(translateInput.value);
            } else user.translations.push(translateInput.value);

            LocalStorage.set(LOGIN_KEY, user);
            const imageArray = Translator.translate(translateInput.value);
            setImages(imageArray);
        }
    }

    return(
        <div style={{backgroundColor: "#ffde9b"}}>
            <Navbar/>
            <input className={"input"} id={"translate-input"}/>
            <button className={"translate"} onClick={translate}>Translate</button>
            <br/>
            {error && <p className={"errorStyle"}>{error}</p>}
            <br/>
            <section className={"signs"}>
                {images && images.map((path, index) => {
                    return <img key={index} src={path} alt={""}/>
                })}
            </section>
        </div>
    )
}

export default Translate;