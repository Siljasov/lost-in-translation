import './Profile.css';
import {LocalStorage} from "../util/localstorage";
import {LOGIN_KEY} from '../Login/Login.js'
import {Link, useHistory} from "react-router-dom";
import {AppRoutes} from "../const/AppRoutes";
import Navbar from "../Navbar/Navbar";

function Profile()
{
    const history = useHistory();
    function Logout()
    {
        LocalStorage.clear();
        history.push(AppRoutes.Login);
    }
    const user = LocalStorage.get(LOGIN_KEY);
    return(
        <div>
            <Navbar/>
            <section className={"profileContent"}>
                <h2>Account Name: {LocalStorage.get(LOGIN_KEY).username}</h2>
                <h5>Translation history</h5>
                <section>
                    {user.translations.map((value, index) => {
                        return <label key={index}>{(index + 1) + ". " + value}<br/> </label>
                    })}
                </section>
                <button className={"profileButton"} onClick={Logout}>Sign Out</button>
                <Link to={AppRoutes.Translate}><button className={"profileButton"}>Translator</button></Link>
            </section>
        </div>
    )
}

export default Profile;