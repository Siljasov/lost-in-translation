export const Translator =
{
    translate(str)
    {
        let signLanguage = [];
        const text = str.toLowerCase();
        for (let i = 0; i < text.length; i++)
        {
            switch(text.charAt(i))
            {
                case 'a': signLanguage.push('./resources/a.png'); break;
                case 'b': signLanguage.push('./resources/b.png'); break;
                case 'c': signLanguage.push('./resources/c.png'); break;
                case 'd': signLanguage.push('./resources/d.png'); break;
                case 'e': signLanguage.push('./resources/e.png'); break;
                case 'f': signLanguage.push('./resources/f.png'); break;
                case 'g': signLanguage.push('./resources/g.png'); break;
                case 'h': signLanguage.push('./resources/h.png'); break;
                case 'i': signLanguage.push('./resources/i.png'); break;
                case 'j': signLanguage.push('./resources/j.png'); break;
                case 'k': signLanguage.push('./resources/k.png'); break;
                case 'l': signLanguage.push('./resources/l.png'); break;
                case 'm': signLanguage.push('./resources/m.png'); break;
                case 'n': signLanguage.push('./resources/n.png'); break;
                case 'o': signLanguage.push('./resources/o.png'); break;
                case 'p': signLanguage.push('./resources/p.png'); break;
                case 'q': signLanguage.push('./resources/q.png'); break;
                case 'r': signLanguage.push('./resources/r.png'); break;
                case 's': signLanguage.push('./resources/s.png'); break;
                case 't': signLanguage.push('./resources/t.png'); break;
                case 'u': signLanguage.push('./resources/u.png'); break;
                case 'v': signLanguage.push('./resources/v.png'); break;
                case 'w': signLanguage.push('./resources/w.png'); break;
                case 'x': signLanguage.push('./resources/x.png'); break;
                case 'y': signLanguage.push('./resources/y.png'); break;
                case 'z': signLanguage.push('./resources/z.png'); break;
                default: break;
            }
        }
        return signLanguage;
    },

}