import {AppRoutes} from "../const/AppRoutes";
import {Link} from "react-router-dom"
import Navbar from "../Navbar/Navbar";

function NotFound()
{
    return(
        <div>
            <Navbar/>
            <h2 style={{left: "5%", position: "relative"}}>Oops, looks like this page doesn't exist.</h2>
            <Link style={{left: "5%", position: "absolute"}} to={AppRoutes.Translate}>Go Back</Link>
        </div>
    )
}

export default NotFound;