import './App.css';
import {
    BrowserRouter,
    Switch
} from 'react-router-dom'
import Login from "./Login/Login";
import Profile from "./Profile/Profile";
import PublicRoute from "./HOC/PublicRoute";
import PrivateRoute from "./HOC/PrivateRoute";
import {AppRoutes} from "./const/AppRoutes";
import NotFound from "./NotFound/NotFound";
import Translate from "./Translate/Translate";

function App()
{
  return (
      <BrowserRouter>
        <div className="App">
        <Switch>
            <PublicRoute path={AppRoutes.Login} exact component={Login}/>
            <PrivateRoute path={AppRoutes.Profile} component={Profile}/>
            <PrivateRoute path={AppRoutes.Translate} component={Translate}/>
            <PublicRoute path={'*'} component={NotFound}/>
        </Switch>
        </div>
      </BrowserRouter>
  );
}

export default App;
